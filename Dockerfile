FROM python:3.6.5-alpine

COPY requirements.freeze /requirements.freeze
RUN pip install -r /requirements.freeze

COPY bitrise_trigger.py /usr/bin/bitrise-trigger

CMD [ "bitrise-trigger", "--help" ]
